package candidateRecruiting.recruitment;

import candidateRecruiting.candidate.Candidate;

public interface Evaluator {

    void evaluate (Candidate candidate);
}
