package candidateRecruiting.candidate;

public enum CandidateStatus {
    ACCEPTED,
    REJECTED,
    AWAITING
}
