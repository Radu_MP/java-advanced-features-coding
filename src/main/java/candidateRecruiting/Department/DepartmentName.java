package candidateRecruiting.Department;

public enum DepartmentName {
    MARKETING,
    PRODUCTION,
    HR,
    IT,
}
